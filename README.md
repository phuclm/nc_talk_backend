# Nextcloud Talk Backend

**Important: This project is not finished and still not working very well. It is supposed to be a proof-of-concept and not (yet) stable enough to be used in production environments!**

Since the default internal signaling was not usable on my system and Nextcloud's own High-Performance-Backend is not affordable by private users, I decided to take a shot and implement a signaling server based on what can be found in the source of Nextcloud Talk and the API documentation.

## Signaling Server

The signaling server itself communicates over a WebSocket connection. For better performance it is recommended to use a separate MCU/SFU. I worked with Janus but I try to keep the interface abstract so it should be possible to expand the connectors for using other servers (Jitsi, etc.)

## How to run

### Docker

There is a pre-made [docker-compose.yml](docker-compose.yml). Modify the environment variables in the file to your needs then run the following commands:

```
docker-compose build
docker-compose up -d
```
