import * as randomstring from 'randomstring';
import * as crypto from 'crypto';
import Axios from 'axios';

import * as config from '../../config.json';

import {BackendRequestType, IBackendRoomResponse, IBackendPingRequest, IBackendRoomRequest, IBackendAuthRequest, IBackendAuthResponse, IBackendPingResponse} from '../types/BackendRequests';

export class BackendApiRequestHelper {

  private url: string = '';

  public setUrl(url: string): void {
    this.url = url;
  }

  public async sendAuthRequest(data: IBackendAuthRequest['auth']): Promise<IBackendAuthResponse['auth']> {
    const requestBody: IBackendAuthRequest = {
      type: BackendRequestType.AUTH,
      auth: {
        version: '1.0',
        params: data.params
      }
    };

    const backendResponse = await this.sendBackendAPIRequest<IBackendAuthRequest, IBackendAuthResponse>(requestBody);

    return backendResponse.auth;
  }

  public async sendRoomRequest(data: IBackendRoomRequest['room']): Promise<IBackendRoomResponse['room']> {
    const requestBody: IBackendRoomRequest = {
      type: BackendRequestType.ROOM,
      room: {
        version: '1.0',
        roomid: data.roomid,
        userid: data.userid,
        sessionid: data.sessionid,
        action: data.action
      }
    };

    const backendResponse = await this.sendBackendAPIRequest<IBackendRoomRequest, IBackendRoomResponse>(requestBody);

    return backendResponse.room;
  }

  public async sendPingRequest(data: IBackendPingRequest['ping']): Promise<IBackendPingResponse['room']> {
    // TODO - find out what and why?
    const requestBody: IBackendPingRequest = {
      type: BackendRequestType.PING,
      ping: {
        roomid: data.roomid,
        entries: data.entries
      }
    };

    const backendResponse = await this.sendBackendAPIRequest<IBackendPingRequest, IBackendPingResponse>(requestBody);

    return backendResponse.room;
  }

  private async sendBackendAPIRequest<T, U>(data: T): Promise<U> {
    const randomString = randomstring.generate(32);

    const hmac = crypto.createHmac('sha256', process.env.SHARED_SECRET || config.sharedSecret);
    hmac.update(randomString);
    hmac.update(JSON.stringify(data));
    const checksum = hmac.digest('hex');

    const response = await Axios.post(this.url, data, {
      headers: {
        'Spreed-Signaling-Random': randomString,
        'Spreed-Signaling-Checksum': checksum,
        'OCS-APIRequest': true
      }
    });

    const responseData = response.data.ocs.data;

    return new Promise((res, rej) => {
      if(responseData.error) {
        rej(responseData.error);
      } else {
        res(responseData);
      }
    });
  }

}
