import {Janus, Session as JanusSession, VideoRoomPluginListenerHandle, VideoRoomPluginPublisherHandle, VideoRoomPluginHandle} from  'janus-videoroom-client';
import {AbstractWebRtcServerHelper} from '../AbstractWebRtcServerHelper';

export class JanusWebRtcServerHelper implements AbstractWebRtcServerHelper {

  private janusClient: Janus;

  private defaultSession: JanusSession|null = null;

  private sessions: Map<string, ISession> = new Map();

  constructor(options: IJanusOptions) {
    this.janusClient = new Janus(options);

    this.janusClient.onConnected(this.onConnectedHandler.bind(this));
    this.janusClient.onDisconnected(this.onDisconnectedHandler.bind(this));
    this.janusClient.onError(this.onErrorHandler.bind(this));

    this.janusClient.onEvent(this.onEventHandler.bind(this));

    this.connect();
  }

  private onConnectedHandler(): void {
    console.log('Janus: connected');
  }

  private onDisconnectedHandler(): void {
    console.log('Janus: disconnected, reconnecting...');
    this.connect();
  }

  private onErrorHandler(): void {
    throw new Error('Janus: could not connect!');
  }

  private onEventHandler(event): void {
    console.log('JANUS EVENT:', event);
  }

  private connect(): void {
    this.janusClient.connect();
  }

  public async getDefaultSession(): Promise<JanusSession> {
    if(!this.defaultSession) {
      this.defaultSession = await this.janusClient.createSession();
    }
    return this.defaultSession;
  }

  public async createSession(sessionId: string): Promise<void> {
    const janusSession = await this.janusClient.createSession();

    this.sessions.set(sessionId, {
      id: sessionId,
      janusSession: janusSession,
      roomId: null,
      publisherHandle: null,
      listenerHandles: new Map()
    });
  }

  public async destroySession(sessionId: string): Promise<void> {
    const sessionToDestroy = this.sessions.get(sessionId);
    if(!sessionToDestroy) throw new SessionError('session does not exist');

    this.unpublish(sessionId);
    sessionToDestroy.listenerHandles.forEach((listenerHandle, subscribedSessionId) => {
      this.unsubscribe(sessionId, subscribedSessionId);
    });

    await this.janusClient.destroySession(Number(sessionId));
  }

  public async getRoomInfos(): Promise<Array<RoomInfo>> {
    const defaultHandle = await this.getDefaultHandle();
    const roomList = await defaultHandle.list();
    return roomList.list;
  }

  private async getRoomInfo(roomId: string): Promise<RoomInfo|null> {
    const roomInfos = await this.getRoomInfos();
    return roomInfos.find(roomInfo => roomInfo.description === roomId) || null;
  }

  public async createRoom(roomId: string): Promise<> {
    const defaultHandle = await this.getDefaultHandle();
    const room = await defaultHandle.create({
      description: roomId,
      is_private: false,
      publishers: 100
    });
    console.log('JANUS: room created', roomId);
    return room.room;
  }

  public async getOrCreateRoom(roomId: string): Promise<> {
    const roomList = await this.getRoomInfos();
    const room = roomList.find(roomInfo => roomInfo.description === roomId);
    if(!room) {
      return await this.createRoom(roomId);
    } else {
      return room;
    }
  }

  public async joinRoom(sessionId: string, roomId: string): Promise<void> {
    const session = this.getSession(sessionId);
    const room = await this.getOrCreateRoom(roomId);
    session.roomId = roomId;
  }

  public async leaveRoom(sessionId: string): Promise<void> {
    const session = this.getSession(sessionId);
    this.unpublish(sessionId);
    session.roomId = null;
  }

  public async deleteRoom(roomId: string): Promise<void> {
    const roomInfo = this.getRoomInfo(roomId);

    const sessionsInRoom = this.getSessionsInRoomId(roomId);
    sessionsInRoom.forEach(session => {
      this.leaveRoom(session.id);
    });

    const defaultHandle = await this.getDefaultHandle();
    defaultHandle.destroy({ room: roomInfo.room });
  }

  public async publishInRoom(sessionId: string, roomId: string, offerSdp: string): Promise<string> {
    this.log(`Publish in room ${roomId}`, sessionId);

    const session = this.getSession(sessionId);

    const room = await this.getRoomInfo(roomId);
    if(!room) throw new RoomError(roomId);

    const publisherHandle = await session.janusSession.videoRoom().publishFeed(room.room, offerSdp);
    session.publisherHandle = publisherHandle;

    return publisherHandle.getAnswer();
  }

  private async getFeedsOfRoom(sessionId: string, roomId: string): Promise<Array<number>> {
    const session = this.getSession(sessionId);

    const room = await this.getRoomInfo(roomId);
    if(!room) throw new RoomError(roomId);

    return await session.janusSession.videoRoom().getFeeds(room.room);
  }

  public async subscribeToFeedOfRoom(sessionId: string, roomId: string, sessionIdToSubscribe: string): Promise<string> {
    this.log(`Subscribe to ${sessionIdToSubscribe} of ${roomId}`, sessionId);

    const session = this.getSession(sessionId);

    const room = await this.getRoomInfo(roomId);
    if(!room) throw new RoomError(roomId);

    const feedId = this.getFeedIdFromSessionId(sessionIdToSubscribe);
    if(!feedId) throw new Error('feed does not exist');

    const listenerHandle = await session.janusSession.videoRoom().listenFeed(room.room, feedId);
    session.listenerHandles.set(sessionIdToSubscribe, listenerHandle);

    return listenerHandle.getOffer();
  }

  public async setAnswerToSubscription(sessionId: string, sessionIdToSubscribe: string, answerSdp: string): Promise<void> {
    this.log(`Answer to ${sessionIdToSubscribe}`, sessionId);

    const session = this.getSession(sessionId);

    const listenerHandle = session.listenerHandles.get(sessionIdToSubscribe);
    if(!listenerHandle) throw new Error('not subscribed to feed!');

    await listenerHandle.setRemoteAnswer(answerSdp);
  }

  async unpublish(sessionId: string) {
    const session = this.getSession(sessionId);
    if(session.publisherHandle) {
      session.publisherHandle.unpublish();
    }
  }

  public async unsubscribe(sessionId: string, sessionIdToUnsubscribe: string): Promise<void> {
    const session = this.getSession(sessionId);

    const listenerHandle = session.listenerHandles.get(sessionIdToUnsubscribe);
    listenerHandle?.leave();

    session.listenerHandles.delete(sessionIdToUnsubscribe);
  }

  public async trickleCandidate(sessionId: string, candidate: ICandidate): Promise<void> {
    const handle = await this.getDefaultHandleOfSession(sessionId);
    handle.trickle(candidate);
  }

  public async trickleCandidates(sessionId: string, candidates: Array<ICandidate>): Promise<void> {
    const handle = await this.getDefaultHandleOfSession(sessionId);
    handle.trickles(candidates);
  }

  public async trickleCompleted(sessionId: string): Promise<void> {
    const handle = await this.getDefaultHandleOfSession(sessionId);
    handle.trickleCompleted();
  }

  private getSession(sessionId: string): ISession {
    const session = this.sessions.get(sessionId);
    if(!session) throw new SessionError();

    return session;
  }

  private getSessionsInRoomId(roomId: string): Array<ISession> {
    const sessionsInRoom: Array<ISession> = [];
    this.sessions.forEach(session => {
      if(session.roomId === roomId) sessionsInRoom.push(session);
    });
    return sessionsInRoom;
  }

  private async getDefaultHandleOfSession(sessionId: string): Promise<VideoRoomPluginHandle> {
    const session = this.getSession(sessionId);
    return await session.janusSession.videoRoom().defaultHandle();
  }

  private async getDefaultHandle(): Promise<VideoRoomPluginHandle> {
    const session = await this.getDefaultSession();
    return await session.videoRoom().defaultHandle();
  }

  private getFeedIdFromSessionId(sessionId: string): number|undefined {
    const session = this.getSession(sessionId);
    return session.publisherHandle?.getPublisherId();
  }

  private log(message: string, data?: any): void {
    console.log(`JANUS HELPER: ${message}`, data || '');
  }

}

interface ISession {
  id: string,
  janusSession: JanusSession,
  roomId: string|null,
  publisherHandle: VideoRoomPluginPublisherHandle|null,
  listenerHandles: Map<string, VideoRoomPluginListenerHandle>
}

interface IJanusOptions {
  url: string,
  apiSecret: string|null
}

// TODO check if necessary
interface ICandidate {
  sdpMid: string,
  sdpMLineIndex: number,
  candidate: string
}

class SessionError extends Error {
  // TODO
}

class RoomError extends Error {

  private _roomId: string

  constructor(roomId: string) {
    super();
    this.message = 'Room does not exist!';
    this._roomId = roomId;
  }

  public getRoomId(): string {
    return this._roomId;
  }

}
