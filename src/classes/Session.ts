import WebSocket from 'ws';
import {v4 as uuid} from 'uuid';

import {BackendApiRequestHelper} from './BackendApiRequestHelper';
import {MessageType, IClientMessage, IServerMessage, IMessageSessionRecipient} from '../types/Events';
import {ClientMessageType} from '../types/SignalingMessages';
import {IBackendRoomRequest} from '../types/BackendRequests';

export class Session {

  private id: string;
  private socket: WebSocket|null;

  private resumeId: string;

  private sessionClosedTimeout?: NodeJS.Timeout;
  private messageQueue: Array<Object> = [];

  private ncUserId?: string;
  private ncRoomId?: string;
  private ncSessionId?: string;

  private eventHandlers: Map<EventName, Array<EventHandler<EventName>>> = new Map();

  private backendApiRequestHelper: BackendApiRequestHelper;

  constructor(options: IConstructorOptions, services: IConstructorServices) {
    this.id = options.sessionId;
    this.socket = options.socket;

    this.resumeId = uuid();

    this.backendApiRequestHelper = services.backendApiRequestHelper;

    this.setupSocket(this.socket);
  }

  public getId(): string {
    return this.id;
  }

  public getResumeId(): string {
    return this.resumeId;
  }

  public getUserId(): string|null {
    return this.ncUserId || null;
  }

  public getRoomId(): string|null {
    return this.ncRoomId || null;
  }

  public getNcSessionId(): string|null {
    return this.ncSessionId || null;
  }

  public setUserId(userId: string): void {
    this.ncUserId = userId;
  }

  private setupSocket(socket: WebSocket): void {
    socket.on('open', this.handleSocketOpened.bind(this));
    socket.on('message', this.handleSocketMessage.bind(this));
    socket.on('close', this.handleSocketClosed.bind(this));
  }

  private closeSocket(socket: WebSocket): void {
    socket.close();
    this.fireEvent('socketClosed', {});
  }

  private handleSocketOpened(): void {
    this.messageQueue.forEach(message => {
      this.sendSocketMessage(message);
    });
  }

  private handleSocketClosed(): void {
    this.log('socket closed');
    this.socket = null;
    this.sessionClosedTimeout = setTimeout(() => {
      this.fireEvent('socketClosed', {});
    }, 10000);
  }

  public resumeSession(socket: WebSocket): void {
    this.log('resume session');
    if(this.sessionClosedTimeout) {
      clearTimeout(this.sessionClosedTimeout);
    }

    this.socket = socket;
    this.setupSocket(this.socket);

    this.resumeId = uuid();
  }

  public sendSocketMessage(data: Object): void {
    if(!this.socket) {
      this.messageQueue.push(data);
    } else {
      this.socket.send(JSON.stringify(data));
    }
  }

  private handleSocketMessage(message: string): void {
    const data = JSON.parse(message);

    switch(data.type) {
      case ClientMessageType.BYE:
        this.handleByeMessage(data.id);
        break;

      case ClientMessageType.ROOM:
        this.handleRoomMessage(data.id, data.room);
        break;

      case ClientMessageType.MESSAGE:
        this.handleMessageMessage(data.message);
        break;

      default:
    }
  }

  private handleByeMessage(id: string): void {
    this.log('BYE');

    this.sendSocketMessage({
      id: id,
      type: ClientMessageType.BYE,
      bye: {}
    });

    this.resumeId = '';
    this.socket && this.closeSocket(this.socket);
  }

  private async handleRoomMessage(id: string, data: IRoomMessageData): Promise<void> {
    this.log('ROOM:', data);

    const sessionId = data.sessionid || this.ncSessionId;
    if(!sessionId) throw new Error('session id not defined!');

    const roomId = data.roomid || this.ncRoomId;
    if(!roomId) throw new Error('room id not defined!');

    const action = data.roomid ? 'join' : 'leave';

    const sendData: IBackendRoomRequest['room'] = {
      version: '1.0',
      userid: this.ncUserId,
      sessionid: sessionId,
      roomid: roomId,
      action: action
    };

    try {
      const backendResponse = await this.backendApiRequestHelper.sendRoomRequest(sendData);

      const response = {
        id: id,
        type: 'room',
        room: backendResponse
      };

      this.ncRoomId = data.roomid || undefined;
      this.ncSessionId = action === 'join' ? data.sessionid : undefined;

      this.sendSocketMessage(response);

      if(sendData.action === 'join') {
        this.fireEvent('roomJoined', { roomId: roomId });
      } else {
        this.fireEvent('roomLeft', { roomId: roomId });
      }
    } catch(error) {
      console.error(error);
    }
  }

  private handleMessageMessage(data: IClientMessage['message']): void {
    this.log('MESSAGE:', data);

    const recipient = data.recipient;

    switch(recipient.type) {
      case MessageType.SESSION:
        this.handleMessageToSession(this.id, recipient, data.data);
        break;

      case MessageType.USER:
        this.handleMessageToUser(recipient.userid, data.data);
        break;

      case MessageType.ROOM:
        // if(!this.ncRoomId) throw new Error('session for socket not found!');
        this.handleMessageToRoom(this.ncRoomId, data.data);
        break;

      default:
        console.warn('Message not understood!', data);
    }
  }

  public sendMessage(recipient: IMessageSessionRecipient, data: Object): void {
    const message: IServerMessage = {
      type: ClientMessageType.MESSAGE,
      message: {
        sender: {
          type: MessageType.SESSION,
          sessionid: recipient.sessionid, // TODO: check if user recipient always has a session too
          userid: 'userid' in recipient ? recipient.userid : undefined
        },
        data: data
      }
    };

    this.sendSocketMessage(message);
  }

  private handleMessageToSession(senderSessionId: string, recipient: IMessageSessionRecipient, data: Object): void {
    const sendData: ISessionSessionMessageEvent = {
      senderSessionId: senderSessionId,
      recipient: recipient,
      data: data
    };

    this.fireEvent('sessionMessage', sendData);
  }

  private handleMessageToUser(userId: string, data: Object): void {
    const sendData: ISessionUserMessageEvent = {
      userId: userId,
      data: data
    };

    this.fireEvent('userMessage', sendData);
  }

  private handleMessageToRoom(roomId: string, data: Object): void {
    const sendData: ISessionRoomMessageEvent = {
      roomId: roomId,
      data: data
    };

    this.fireEvent('roomMessage', sendData);
  }

  public on<K extends EventName>(eventName: K, eventHandler: (v: IEvents[K]) => void): void {
    const eventHandlers = this.eventHandlers.get(eventName);
    if(!eventHandlers) {
      this.eventHandlers.set(eventName, [ eventHandler ]);
    } else {
      eventHandlers.push(eventHandler);
    }
  }

  private fireEvent<K extends EventName>(eventName: K, eventData: IEvents[K]): void {
    const eventHandlers = this.eventHandlers.get(eventName);
    eventHandlers?.forEach(handler => {
      handler(eventData);
    });
  }

  private log(message: string, data?: any): void {
    console.log(`SESSION ${this.id}: ${message}`, data || '');
  }

}

interface IConstructorOptions {
  sessionId: string,
  socket: WebSocket
}

interface IConstructorServices {
  backendApiRequestHelper: BackendApiRequestHelper
}

interface IRoomMessageData {
  roomid?: string,
  // Pass the Nextcloud session id to the signaling server. The
  // session id will be passed through to Nextcloud to check if
  // the (Nextcloud) user is allowed to join the room.
  sessionid?: string
}

interface IEvents {
  socketClosed: {},
  sessionMessage: ISessionSessionMessageEvent,
  userMessage: ISessionUserMessageEvent,
  roomMessage: ISessionRoomMessageEvent,
  roomJoined: ISessionRoomJoinedEvent,
  roomLeft: ISessionRoomLeftEvent
}

type EventName = keyof IEvents;

type EventHandler<K extends EventName> = (v: IEvents[K]) => void;

export interface ISessionSessionMessageEvent {
  recipient: IMessageSessionRecipient,
  senderSessionId: string,
  data: any
}

export interface ISessionUserMessageEvent {
  userId: string,
  data: any
}

export interface ISessionRoomMessageEvent {
  roomId: string,
  data: any
}

export interface ISessionRoomJoinedEvent {
  roomId: string
}

export interface ISessionRoomLeftEvent {
  roomId: string
}
