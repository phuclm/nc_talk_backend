export abstract class AbstractWebRtcServerHelper {

  abstract async createSession(sessionId: string): Promise<void>;
  abstract async destroySession(sessionId: string): Promise<void>;

  abstract createRoom(roomId: string): IRoomInfo;
  abstract deleteRoom(roomId: string): void;

  abstract async joinRoom(sessionId: string, roomId: string): Promise<void>;
  abstract async leaveRoom(sessionId: string, roomId: string): Promise<void>;

  abstract async publishInRoom(sessionId: string, roomId: string, offer: Offer): Promise<Answer>;
  abstract async subscribeToFeedOfRoom(sessionId: string, roomId: string, sessionIdToSubscribe: string): Promise<Offer>;
  abstract async setAnswerToSubscription(sessionId: string, sessionIdToSubscribe: string, answer: Answer): Promise<void>;

  abstract async unpublish(sessionId: string): Promise<void>;
  abstract async unsubscribe(sessionId: string, sessionIdToUnsubscribe: string): Promise<void>;

  abstract async trickleCandidate(sessionId: string, candidate: ICandidate): Promise<void>;

  public async trickleCandidates(sessionId: string, candidates: Array<ICandidate>): Promise<void> {
    for(const candidate of candidates) {
      await this.trickleCandidate(sessionId, candidate);
    }
  }

  abstract async trickleCompleted(sessionId: string): Promise<void>;

}

interface IRoomInfo {
  // TODO
}

type Offer = string;
type Answer = string;

interface ICandidate {
  // TODO
}
