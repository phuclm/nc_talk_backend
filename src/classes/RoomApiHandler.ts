import express from 'express';
import {Router, Response, Request} from 'express';

import {RoomApiRequestType, RoomApiRequest, IRoomApiInviteRequestData, IRoomApiUpdateRequestData, IRoomApiDeleteRequestData, IRoomApiParticipantsRequestData, IRoomApiIncallRequestData, IRoomApiMessageRequestData, RoomApiDisinviteRequestData} from '../types/RoomApi';

import {RoomListEventData, RoomListEventType, ParticipantsListEventData, ParticipantsListEventType} from '../types/Events';

export class RoomApiHandler {

  private router: Router;

  private eventHandlers: Map<EventName, Array<EventHandler<EventName>>> = new Map();

  constructor() {
    this.router = express.Router();
    this.setUpRoutes();
  }

  public getRouter(): Router {
    return this.router;
  }

  private setUpRoutes(): void {
    this.router.post('/api/v1/room/:roomId', this.handleRoomApiRequest.bind(this));
  }

  private handleRoomApiRequest(request: Request, response: Response): void {
    const roomId = request.params.roomId;

    const body: RoomApiRequest = request.body;

    switch(body.type) {
      case RoomApiRequestType.INVITE:
        this.handleInviteRequest(roomId, body.invite);
        break;

      case RoomApiRequestType.DISINVITE:
        this.handleDisinviteRequest(roomId, body.disinvite);
        break;

      case RoomApiRequestType.UPDATE:
        this.handleUpdateRequest(roomId, body.update);
        break;

      case RoomApiRequestType.DELETE:
        this.handleDeleteRequest(roomId, body.delete);
        break;

      case RoomApiRequestType.PARTICIPANTS:
        this.handleParticipantsRequest(roomId, body.participants);
        break;

      case RoomApiRequestType.INCALL:
        this.handleIncallRequest(roomId, body.incall);
        break;

      case RoomApiRequestType.MESSAGE:
        this.handleMessageRequest(roomId, body.message);
        break;

      default:
    }

    response.status(200).send();
  }

  // Room::EVENT_AFTER_USERS_ADD
  private handleInviteRequest(roomId: string, data: IRoomApiInviteRequestData): void {
    this.log('INVITE:', data);

    const eventData: RoomListEventData = {
      target: 'roomlist',
      type: RoomListEventType.INVITE,
      invite: {
        roomid: roomId,
        properties: data.properties

      }
    };

    const sendData: RoomApiHandlerRoomListEvent = {
      eventData: eventData,
      userIds: data.userids
    };

    this.fireEvent('roomListEvent', sendData);
  }

  // Room::EVENT_AFTER_USER_REMOVE
  // Room::EVENT_AFTER_PARTICIPANT_REMOVE -> sessions
  private handleDisinviteRequest(roomId: string, data: RoomApiDisinviteRequestData): void {
    this.log('DISINVITE:', data);

    const eventData: RoomListEventData = {
      target: 'roomlist',
      type: RoomListEventType.DISINVITE,
      disinvite: {
        roomid: roomId,
        properties: data.properties
      }
    };

    const sendData: RoomApiHandlerRoomListEvent = {
      eventData: eventData,
      ...('sessionids' in data ? { sessionIds: data.sessionids } : { userIds: data.userids })
    };

    this.fireEvent('roomListEvent', sendData);
  }

  // Room::EVENT_AFTER_NAME_SET
  // Room::EVENT_AFTER_PASSWORD_SET
  // Room::EVENT_AFTER_TYPE_SET
  // Room::EVENT_AFTER_READONLY_SET
  // Room::EVENT_AFTER_LOBBY_STATE_SET
  // TODO remove handler with "roomModified" in favour of handler with
  // "participantsModified" once the clients no longer expect a
  // "roomModified" message for participant type changes.
  // Room::EVENT_AFTER_PARTICIPANT_TYPE_SET
  private handleUpdateRequest(roomId: string, data: IRoomApiUpdateRequestData): void {
    this.log('UPDATE:', data);

    const eventData: RoomListEventData = {
      target: 'roomlist',
      type: RoomListEventType.UPDATE,
      update: {
        roomid: roomId,
        properties: data.properties
      }
    };

    const sendData: RoomApiHandlerRoomListEvent = {
      eventData: eventData,
      userIds: data.userids
    };

    this.fireEvent('roomListEvent', sendData);
  }

  // Room::EVENT_BEFORE_ROOM_DELETE
  private handleDeleteRequest(roomId: string, data: IRoomApiDeleteRequestData): void {
    this.log('DELETE:', data);

    const eventData: RoomListEventData = {
      target: 'roomlist',
      type: RoomListEventType.DISINVITE,
      disinvite: {
        roomid: roomId
      }
    };

    const sendData: RoomApiHandlerRoomListEvent = {
      eventData: eventData,
      userIds: data.userids
    };

    this.fireEvent('roomListEvent', sendData);
    this.fireEvent('roomDeleted', { roomId: roomId });
  }

  // Room::EVENT_AFTER_PARTICIPANT_TYPE_SET
  // Room::EVENT_AFTER_GUESTS_CLEAN
  // GuestManager::EVENT_AFTER_NAME_UPDATE
  private handleParticipantsRequest(roomId: string, data: IRoomApiParticipantsRequestData): void {
    this.log('PARTICIPANTS:', data);

    const eventData: ParticipantsListEventData = {
      target: 'participants',
      type: ParticipantsListEventType.UPDATE,
      update: {
        roomid: roomId,
        users: data.changed
      }
    };

    const sendData: IRoomApiHandlerParticipantsListEvent = {
      eventData: eventData
    };

    this.fireEvent('participantsListEvent', sendData);
  }

  // Room::EVENT_AFTER_SESSION_JOIN_CALL
  // Room::EVENT_AFTER_SESSION_LEAVE_CALL
  private handleIncallRequest(roomId: string, data: IRoomApiIncallRequestData): void {
    this.log('INCALL:', data);

    const eventData: ParticipantsListEventData = {
      target: 'participants',
      type: ParticipantsListEventType.UPDATE,
      update: {
        roomid: roomId,
        users: data.changed
      }
    };

    const sendData: IRoomApiHandlerParticipantsListEvent = {
      eventData: eventData
    };

    this.fireEvent('participantsListEvent', sendData);
  }

  // ChatManager::EVENT_AFTER_MESSAGE_SEND
  // ChatManager::EVENT_AFTER_SYSTEM_MESSAGE_SEND
  private handleMessageRequest(roomId: string, data: IRoomApiMessageRequestData): void {
    this.log('MESSAGE:', data);
    if(data.data && data.data.chat) {
      this.fireEvent('roomMessage', { roomId: roomId, data: data.data });
    }
  }

  public on<K extends EventName>(eventName: K, eventHandler: (v: IEvents[K]) => void): void {
    const eventHandlers = this.eventHandlers.get(eventName);
    if(!eventHandlers) {
      this.eventHandlers.set(eventName, [ eventHandler ]);
    } else {
      eventHandlers.push(eventHandler);
    }
  }

  private fireEvent<K extends EventName>(eventName: K, eventData: IEvents[K]): void {
    const eventHandlers = this.eventHandlers.get(eventName);
    eventHandlers?.forEach(handler => {
      handler(eventData);
    });
  }

  private log(message: string, data?: any): void {
    console.log(`ROOM API: ${message}`, data || '');
  }

}

interface IEvents {
  roomListEvent: RoomApiHandlerRoomListEvent,
  roomMessage: IRoomApiHandlerRoomMessageEvent,
  roomDeleted: IRoomApiHandlerRoomDeletedEvent,
  participantsListEvent: IRoomApiHandlerParticipantsListEvent
}

type EventName = keyof IEvents;

type EventHandler<K extends EventName> = (v: IEvents[K]) => void;

export type RoomApiHandlerRoomListEvent = IRoomApiHandlerRoomListEventCommon & ( IRoomApiHandlerRoomListEventSessionIds | IRoomApiHandlerRoomListEventUserIds );

interface IRoomApiHandlerRoomListEventCommon {
  eventData: RoomListEventData
}

interface IRoomApiHandlerRoomListEventSessionIds {
  sessionIds: Array<string>
}

interface IRoomApiHandlerRoomListEventUserIds {
  userIds: Array<string>
}

export interface IRoomApiHandlerRoomMessageEvent {
  roomId: string,
  data: any
}

export interface IRoomApiHandlerRoomDeletedEvent {
  roomId: string
}

export interface IRoomApiHandlerParticipantsListEvent {
  eventData: ParticipantsListEventData
}
