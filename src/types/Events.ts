import {IParticipant, IRoomApiRoomProperties} from './RoomApi';

interface IEvent {
  type: 'event',
  event: EventData
}

export type EventData = RoomEventData | RoomListEventData | ParticipantsListEventData;



/* Room event */

export type RoomEventData = IRoomEventDataCommon & ( IRoomJoinEventData | IRoomLeaveEventData | IRoomChangeEventData | IRoomMessageEventData );

interface IRoomEventDataCommon {
  target: 'room'
}

interface IRoomJoinEventData {
  type: RoomEventType.JOIN,
  join: Array<{ sessionid: string, userid?: string, user? }>
}

interface IRoomLeaveEventData {
  type: RoomEventType.LEAVE,
  leave: Array<string>
}

interface IRoomChangeEventData {
  type: RoomEventType.CHANGE,
  change: Array // TODO: not documented in NC API docs?
}

interface IRoomMessageEventData {
  type: RoomEventType.MESSAGE,
  message: {
    roomid: string,
    data: {
      type: 'chat',
      chat: {
        refresh: true
      }
    }
  }
}


/* Room list event */

export type RoomListEventData = IRoomListEventDataCommon & ( IRoomListInviteEventData | IRoomListDisinviteEventData | IRoomListUpdateEventData );

interface IRoomListEventDataCommon {
  target: 'roomlist'
}

interface IRoomListInviteEventData {
  type: RoomListEventType.INVITE,
  invite: {
    roomid: string,
    properties: IRoomApiRoomProperties
  }
}

interface IRoomListDisinviteEventData {
  type: RoomListEventType.DISINVITE,
  disinvite: {
    roomid: string
  }
}

interface IRoomListUpdateEventData {
  type: RoomListEventType.UPDATE,
  update: {
    roomid: string,
    properties: IRoomApiRoomProperties
  }
}



/* Participants list event */

export type ParticipantsListEventData = IParticipantsListEventDataCommon & ( IParticipantsListUpdateEventData );

interface IParticipantsListEventDataCommon {
  target: 'participants'
}

interface IParticipantsListUpdateEventData {
  type: ParticipantsListEventType.UPDATE,
  update: {
    roomid: string,
    users: Array<IParticipant>
  }
}



/* Messages */

export interface IClientMessage {
  id: string,
  type: 'message',
  message: {
    recipient: MessageRecipient,
    data: Object
  }
}

export interface IServerMessage {
  type: 'message',
  message: {
    sender: IMessageSender,
    data: Object
  }
}

export type MessageRecipient = IMessageSessionRecipient | IMessageUserRecipient | IMessageRoomRecipient;

export interface IMessageSessionRecipient {
  type: MessageType.SESSION,
  sessionid: string
}

interface IMessageUserRecipient {
  type: MessageType.USER,
  userid: string
}

interface IMessageRoomRecipient {
  type: MessageType.ROOM
}

interface IMessageSender {
  type: MessageType,
  sessionid: string,
  userid?: string
}



/* Enums */

export enum RoomEventType {
  JOIN = 'join',
  LEAVE = 'leave',
  CHANGE = 'change',
  MESSAGE = 'message'
}

export enum RoomListEventType {
  INVITE = 'invite',
  DISINVITE = 'disinvite',
  UPDATE = 'update'
}

export enum ParticipantsListEventType {
  UPDATE = 'update'
}

export enum MessageType {
  SESSION = 'session',
  USER = 'user',
  ROOM = 'room'
}
