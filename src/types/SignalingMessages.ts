export enum ClientMessageType {
  HELLO = 'hello',
  BYE = 'bye',
  ROOM = 'room',
  CONTROL = 'control',
  MESSAGE = 'message'
}

export enum MessageMessageDataType {
  REQUESTOFFER = 'requestoffer',
  SENDOFFER = 'sendoffer'
}

export enum RoomType {
  VIDEO = 'video',
  SCREEN = 'screen'
}
