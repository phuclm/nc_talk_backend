import {IRoomApiRoomProperties} from './RoomApi';

export type BackendRequest = IBackendAuthRequest | IBackendRoomRequest | IBackendPingRequest;

export type BackendResponse = IBackendAuthResponse | IBackendRoomResponse | IBackendPingResponse | IBackendErrorResponse;



/* Backend requests */

export interface IBackendAuthRequest {
  type: BackendRequestType.AUTH,
  auth: {
    version: '1.0',
    params: {
      userid: string,
      ticket: string
    }
  }
}

export interface IBackendRoomRequest {
  type: BackendRequestType.ROOM,
  room: {
    version: '1.0',
    roomid: string,
    userid?: string,
    sessionid: string, // NC session id
    action: 'join' | 'leave'
  }
}

export interface IBackendPingRequest {
  type: BackendRequestType.PING,
  ping: {
    roomid: string,
    entries: Array<{ sessionid: string }>
  }
}

export enum BackendRequestType {
  AUTH = 'auth',
  ROOM = 'room',
  PING = 'ping'
}



/* Backend responses */

export interface IBackendAuthResponse {
  type: BackendRequestType.AUTH,
  auth: {
    version: '1.0',
    userid?: string,
    user?: {
      displayname: string
    }
  }
}

export interface IBackendRoomResponse {
  type: BackendRequestType.ROOM,
  room: {
    version: '1.0',
    roomid: string,
    properties: IRoomApiRoomProperties,
    session?
  }
}

export interface IBackendPingResponse {
  type: BackendRequestType.ROOM,
  room: {
    version: '1.0',
    roomid: string
  }
}



/* Error responses */

interface IBackendErrorResponse {
  type: 'error',
  error: {
    code: BackendErrorResponseCode,
    message: string
  }
}

declare enum BackendErrorResponseCode {
  INVALID_TICKET = 'invalid_ticket',
  NO_SUCH_USER = 'no_such_user',
  NO_SUCH_ROOM = 'no_such_room'
}
